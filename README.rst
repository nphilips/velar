.. include:: <isonum.txt>

ɣelar
======
ɣelar (or Velar) is a personal web application that can render your MarkDown_,
reStructuredText_ and other markup documents (see `Supported File Types`_) as
HTML_.  If you got here by clicking the  about_ link then this page is rendered
from the reStructuredText source_. ɣelar can also navigate of the directory
tree where the documents are located.

ɣelar is simple and clean. The CSS style is based on Bootstrap_ and the icons
are from `Font Awesome`_.  It uses the Pygments_ library to syntax highlight
any supported source code in files or embedded in supported document formats.

ɣelar supports *Live Preview* using Live.js_. If you have the HTML
representation of a document open in your browser and you make changes to the
MarkDown or reStructuredText source, the browser will automatically refresh the
page to show the latest version. Great for side-by-side editing.

ɣelar is fast. Every caching technique we can think of is used to make
sure browsing is quick.

And when you want to share your documents with others, quickly generate
standalone documents that have all the required resources embedded.  Just click
on *Export* and then choose **HTML** or **PDF** [#exportformats]_ or **PNG**
[#exportformats]_.

On Windows, ɣelar integrates with Explorer so you can double click on a
MarkDown or reStructuredText file to see the HTML version in your browser.

This application was inspired by
http://jblevins.org/log/webpy-markdown

.. [#exportformats] PDF and PNG export is still experimental and may not work
                    for everyone.

Supported File Types
--------------------
* `Plain Text`_
* MarkDown_
* reStructuredText_
* Textile_
* Common image file types
* HTML Files
* Any source code files supported by `Pygments Lexers <http://pygments.org/docs/lexers>`_

.. _Markdown: http://daringfireball.net/projects/markdown/
.. _reStructuredText: http://docutils.sourceforge.net/rst.html
.. _Textile: https://en.wikipedia.org/wiki/Textile_%28markup_language%29
.. _Plain Text: https://en.wikipedia.org/wiki/Text_file
.. _HTML: http://en.wikipedia.org/wiki/HTML
.. _Bootstrap: http://getbootstrap.com/
.. _Font Awesome: http://fontawesome.io/
.. _Pygments: http://pygments.org/
.. _about: /about
.. _source: ?format=raw

Installing and Running the Server
---------------------------------
1. Install Python. Windows users may install either the standalone_ version or the one
   provided by Cygwin_.

   * If using Cygwin Python, you'll also need to install setuptools_::

        wget http://peak.telecommunity.com/dist/ez_setup.py
        python ez_setup.py

2. Install required libraries::

    pip install web.py markdown slimmer pyScss pillow user_agents

3. Start the server::

    python app.py

4. If you are using Cygwin_ on Windows, you may use ``service.sh``
   script to create a Windows Service that can run ɣelar in the
   background.

   To run this script, you need to start the Cygwin Terminal as an Administrator.

To compile the launcher you'll need a C compiler and Windows API headers. We
compile it with the Cygwin GCC.

.. _standalone: http://www.python.org/getit/windows/
.. _Cygwin: http://www.cygwin.com/
.. _setuptools: http://pypi.python.org/pypi/setuptools

Configuration
-------------
User configurable settings are stored in a file named ``settings.py``. All
available settings are listed in the file.

One setting you must change is the ``content_root``. Set this to the full path
where your documents are stored.

Browser Support
---------------
.. Hint:: You can tell the browsers to not cache the pages by setting the
          ``allow_cache`` setting to False.

Firefox
    Make sure that the ``browser.cache.disk.enable`` setting is set to
    ``True``. Otherwise Firefox will not send the ``If-Modified-Since`` header
    to the server. Without this header, the server will have to render the
    documents each time you load it.

Security Concerns
-----------------
If you run ``python app.py`` the server will bind to IP address at ``0.0.0.0``
and will be available to anyone who can reach your computer. To limit access to
only the localhost, you can bind to ``127.0.0.1``. To do that run the server
like this: ``python app.py 127.0.0.1``.

The ``install_service.sh`` script will bind to ``127.0.0.1`` by default and is
only available locally.

Features
========
Going to the Index page at http://localhost:8080/ will list all available text
files that can be rendered. Either MarkDown or reStructuredText files can be rendered.

ɣelar Specific Features
-----------------------------
Titles
``````
ɣelar uses a number of techniques to identify a document title. If all
the techniques fail, it will use the file name as the title.

The first header 
    ɣelar reads the first 2 lines of the document and if the second line
    starts with ``=``, ɣelar will assume that the first line is the title
    and use that. When rendering index pages, for performance reasons, this is
    the only method used to figure out the document title.

The reStructuredText title
    The reStructuredText parser takes the first header to be the document
    title. When rendering documents, the ɣelar will use this.

The MarkDown title
    The MarkDown parser itself does not support titles, however ɣelar can
    use the title specified using the Meta-Data_ extension syntax.

`Live.js <http://livejs.com/>`_
-------------------------------
Pages include the Live.js script to allow automatic reloading when the content
is changed. When the rst or markdown file is modified, the browser will
automatically reload the page.

Markdown Extensions
-------------------
The python markdown parser supports some extensions to the vanilla markdown
syntax. The following are enabled by default in ɣelar:

* `Abbreviations <http://packages.python.org/Markdown/extensions/abbreviations.html>`_::

    The HTML specification 
    is maintained by the W3C.

    *[HTML]: Hyper Text Markup Language
    *[W3C]:  World Wide Web Consortium

* `Attribute Lists <http://packages.python.org/Markdown/extensions/attr_list.html>`_ (affects the HTML output)::

    This is a paragraph.
    {: #an_id .a_class }

* `Definition Lists <http://packages.python.org/Markdown/extensions/definition_lists.html>`_::

    Apple
    :   Pomaceous fruit of plants of the genus Malus in 
        the family Rosaceae.

    Orange
    :   The fruit of an evergreen tree of the genus Citrus.

* `Fenced Code Blocks <http://packages.python.org/Markdown/extensions/fenced_code_blocks.html>`_::

    This is a paragraph introducing:

    ~~~~~~~~~~~~~~~~~~~
    a one-line code block
    ~~~~~~~~~~~~~~~~~~~~

* `Footnotes <http://packages.python.org/Markdown/extensions/footnotes.html>`_::

    Footnotes[^1] have a label[^label] and a definition[^!DEF].

    [^1]: This is a footnote
    [^label]: A footnote on "label"
    [^!DEF]: The definition of a footnote.

* `Tables <http://packages.python.org/Markdown/extensions/tables.html>`_::

    First Header  | Second Header
    ------------- | -------------
    Content Cell  | Content Cell
    Content Cell  | Content Cell

* `Smart Strong <http://packages.python.org/Markdown/extensions/smart_strong.html>`_. Allows you to enter
  double underscores in words, without having them parsed as Strong markup.
* `CodeHilite <http://packages.python.org/Markdown/extensions/code_hilite.html>`_ (using Pygments):

  .. code:: python

    #!/usr/bin/python
    # Code goes here ...

  or:

  .. code:: python

    #!python
    # Code goes here ...

* `Meta-Data <http://packages.python.org/Markdown/extensions/meta_data.html>`_.
  It can be placed at the top of the file::

    Title:   My Document
    Summary: A brief description of my document.
    Authors: Waylan Limberg
            John Doe
    Date:    October 2, 2007
    blank-value: 
    base_url: http://example.com

    This is the first paragraph of the document.

  ɣelar will use the Title meta-data to set the page title. If it's
  missing, the first line of the file is used as the title.

* `Table of Contents <http://packages.python.org/Markdown/extensions/toc.html>`_. Place this anywhere in the document:

  .. code:: moin

    [TOC]

* `WikiLinks <http://packages.python.org/Markdown/extensions/wikilinks.html>`_:

  .. code:: moin

    [[Bracketed]]

  will be turned into:

  .. code:: html

    <a href="/Bracketed/" class="wikilink">Bracketed</a>

  .. note:: Since ɣelar requires the file extension to be included in the URL,
            This extension will not be very useful in creating internal links.
            Support for automatically figuring out the file extensions is planned.
