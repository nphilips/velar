/*
 *
 */
#include <curl/curl.h>

#include <sys/stat.h>
#include <fcntl.h>
#include <windows.h>
#include <stdio.h>
#include <Shlobj.h>

#include "ini/ini.h"
#include "bstrlib/bstrlib.h"

#define SETTINGS_FRAGMENT "\\TextServerLauncher\\Settings.ini"

//#define DEBUG

#define 	_O_BINARY   0x8000

typedef struct
{
    const char* url;
    const char* fileroot;
    const char* url1;
    const char* fileroot1;
} configuration;


static int handler(void* user, const char* section, const char* name,
                   const char* value)
{
    configuration* pconfig = (configuration*)user;

    #define MATCH(s, n) strcmp(section, s) == 0 && strcmp(name, n) == 0

    if (MATCH("textserver", "url"))
    {
        pconfig->url = strdup(value);
    } else if (MATCH("textserver", "fileroot"))
    {
        pconfig->fileroot = strdup(value);
    }
    if (MATCH("textserver1", "url"))
    {
        pconfig->url1 = strdup(value);
    } else if (MATCH("textserver1", "fileroot"))
    {
        pconfig->fileroot1 = strdup(value);
    } else {
        return 0;  /* unknown section/name, error */
    }
    return 1;
}


const char* output_file;
static FILE * tempHTMLfile;

/*
 * This function is called by libCurl each time it receives a chunk of data.
 */
size_t save_response( char *buffer, size_t size, size_t nmemb, void *userdata)
{
    static int first_time=1;
    static bstring filename;

    if (first_time) {
        first_time = 0;

        TCHAR lpTempPathBuffer[MAX_PATH];
        TCHAR szTempFileName[MAX_PATH];

        GetTempPath(MAX_PATH, lpTempPathBuffer);
        GetTempFileName(lpTempPathBuffer, TEXT("tsv"), 0, szTempFileName);

        filename = bfromcstr(szTempFileName);

        bconcat(filename, bfromcstr(".html"));

        printf("First time. Using temp file: %s\n", filename->data);

        tempHTMLfile = fopen (filename->data, "wb");
        output_file = filename->data;
    }

    size_t written = fwrite (buffer , size, size * nmemb, tempHTMLfile);

    return written;
}

int request(const char * filename, const char * url)
{
    FILE * pFile;
    unsigned long  * buffer;
    size_t result;

    printf("About to render %s\n", filename);

    struct stat info;
    stat(filename, &info);
    printf("STAT FILE SIZE: %d\n", info.st_size);

    // obtain file size:
    int fd;
    off_t file_size;
    struct stat st;

    fd = open(filename, O_RDONLY);
    if (fd == -1) {
        /* Handle error */
    }

    pFile = fdopen(fd, "r");
    if (pFile == NULL) {
        /* Handle error */
    }

    int modeResult = _setmode(fileno(pFile), _O_BINARY );
    if( modeResult == -1 )
        perror( "Cannot set mode" );
    else
        printf( "'file' successfully changed to binary mode\n" );

    /* Ensure that the file is a regular file */
    if ((fstat(fd, &st) != 0) || (!S_ISREG(st.st_mode))) {
        /* Handle error */
    }

    if (fseeko(pFile, 0 , SEEK_END) != 0) {
        /* Handle error */
    }

    file_size = ftello(pFile) ;
    if (file_size == -1) {
        /* Handle error */
    }

    printf("FTELLO FILE SIZE: %d\n", info.st_size);

    fseek(pFile, 0, SEEK_SET);

    // allocate memory to contain the whole file:
    buffer = (unsigned long *) malloc (file_size);
    if (buffer == NULL) {fputs ("Memory error",stderr); exit (2);}

    // copy the file into the buffer:
    result = fread (buffer,1,file_size,pFile);
    if (result != file_size) {fputs ("Reading error",stderr); exit (3);}

    /* the whole file is now loaded in the memory buffer. */

    // terminate
    fclose (pFile);

    #if defined(DEBUG)
        printf("%s\n", buffer);
    #endif

    CURL *curl;
    curl_global_init(CURL_GLOBAL_ALL);
    curl = curl_easy_init();

    char *filebasename = (char *)basename(filename);


    bstring posturl = bfromcstr(url);
    bconcat(posturl, bfromcstr("/rendertext?filename="));
    bconcat(posturl, bfromcstr(curl_easy_escape(curl, filebasename, strlen(filebasename))));

    curl_easy_setopt(curl, CURLOPT_WRITEFUNCTION, &save_response);
    curl_easy_setopt(curl, CURLOPT_POSTFIELDS, buffer);
    curl_easy_setopt(curl, CURLOPT_POSTFIELDSIZE, info.st_size);
    curl_easy_setopt(curl, CURLOPT_URL, posturl->data);

    printf("POSTing to %s\n", posturl->data);

    curl_easy_perform(curl); /* post away! */

    free (buffer);
    fclose (tempHTMLfile);
    return 0;
}

/*
 * This application takes a single command-line argument, the full path
 * to the file to show in Text Server.
 *
 * The application trims off the known root folder part from the command-line
 * and builds the url to text server. It then launches the URL to show
 * the rendered version of the document.
 *
 * Future improvements:
 *   * Pass the file along to some other application if the file
 *     is not in the pre-defined root folder.
 */
int WINAPI WinMain(HINSTANCE hInstance, HINSTANCE hPrevInstance,
        LPSTR lpCmdLine, int nCmdShow)
{

    printf("CommandLine: \"%s\"\n", lpCmdLine);



    configuration config;

    TCHAR szPath[MAX_PATH];
    if (!SUCCEEDED(SHGetFolderPath(NULL, CSIDL_APPDATA, NULL, 0, szPath)))
    {
        printf("Error. Can't get to the APPDATA folder path.\n");
    }

    bstring settings_file = bfromcstr(szPath);
    bconcat(settings_file, bfromcstr(SETTINGS_FRAGMENT));

    if (ini_parse(settings_file->data, handler, &config) < 0)
    {
        printf("Can't load '%s'\n", settings_file->data);
        return 1;
    }

    /*
     * Sanitize input
     */

    // This is the Windows path to the root of your Text Server.
    const bstring lpRoot = bfromcstr(config.fileroot);
    if(lpRoot->data[lpRoot->slen - 1] != '\\')
    {
        bconcat(lpRoot, bfromcstr("\\"));
    }

    const bstring lpRoot1 = bfromcstr(config.fileroot1);
    if(lpRoot1->data[lpRoot1->slen - 1] != '\\')
    {
        bconcat(lpRoot1, bfromcstr("\\"));
    }

    // URL cannot end in a '/'. Remove it if it does
    bstring url = cstr2bstr(config.url);
    if(url->data[url->slen - 1] == '/')
    {
        bdelete(url, url->slen - 1, 1);
    }

    bstring url1 = cstr2bstr(config.url1);
    if(url1->data[url1->slen - 1] == '/')
    {
        bdelete(url1, url1->slen - 1, 1);
    }

    printf("Using File Root: %s\n", lpRoot->data);
    printf("Using URL: %s\n", url->data);

    printf("Using File Root 1: %s\n", lpRoot1->data);
    printf("Using URL 1: %s\n", url1->data);

    bstring lpFilePath = bfromcstr(lpCmdLine);
    btrimws(lpFilePath); // trim any whitespace

    if(lpFilePath->slen <= 0)
    {
        printf("Not enough arguments.\n");
        printf("Usage: launcher.exe [textfile].\n");
        return 1;
    }

    if(lpFilePath->data[0] == '"')
    {
        // If the input is quoted, shave off the first and last character
        printf("Stripping quotes\n");
        printf("File Path Before: %s\n", lpFilePath->data);
        bdelete(lpFilePath, 0, 1);
        bdelete(lpFilePath, lpFilePath->slen - 1, 1);
        printf("File Path After: %s\n", lpFilePath->data);
    }

    printf("Root: %s\n", lpRoot->data);
    printf("File Path: %s\n", lpFilePath->data);

    // Check if the given file is in the text server root
    if(binstrcaseless(lpFilePath, 0, lpRoot) == 0)
    {
        printf("File can be accessed by the server.\n");
        printf("Before: %s\n", lpFilePath->data);

        bstring bFileFragment = bstrcpy(lpFilePath);

        // Get the relative path to the file in the server
        // ! Our comparison is case-insentive. This is not!
        bfindreplace(bFileFragment, lpRoot, bfromcstr(""), 0);
        // \ -> /
        bfindreplace(bFileFragment, bfromcstr("\\"), bfromcstr("/"), 0);

        printf("After: %s\n", bFileFragment->data);

        // Prefix the file stub with the URL to the server.
        bconcat(url, bfromcstr("/"));
        bconcat(url, bFileFragment);

        // Finally tell explorer to open the default browser and navigate to this URL.
        printf("Opening: %s\n", url->data);
        ShellExecute(NULL, "open", url->data, NULL, NULL, SW_SHOWNORMAL);

    }

        // Check if the given file is in the text server root 1
    if(binstrcaseless(lpFilePath, 0, lpRoot1) == 0)
    {
        printf("File can be accessed by the server.\n");
        printf("Before: %s\n", lpFilePath->data);

        bstring bFileFragment = bstrcpy(lpFilePath);

        // Get the relative path to the file in the server
        // ! Our comparison is case-insentive. This is not!
        bfindreplace(bFileFragment, lpRoot1, bfromcstr(""), 0);
        // \ -> /
        bfindreplace(bFileFragment, bfromcstr("\\"), bfromcstr("/"), 0);

        printf("After: %s\n", bFileFragment->data);

        // Prefix the file stub with the URL to the server.
        bconcat(url1, bfromcstr("/"));
        bconcat(url1, bFileFragment);

        // Finally tell explorer to open the default browser and navigate to this URL.
        printf("Opening: %s\n", url1->data);
        ShellExecute(NULL, "open", url1->data, NULL, NULL, SW_SHOWNORMAL);

    }

//    else
//    {
//        printf("File cannot be accessed by the server.\n");
//        request(lpFilePath->data, url->data);
//
//        // Finally tell explorer to open the default browser and navigate to this URL.
//        printf("Opening: %s\n", output_file);
//        ShellExecute(NULL, "open", output_file, NULL, NULL, SW_SHOWNORMAL);
//    }

#if defined(DEBUG)
    while(1);
#endif
    // All done.
    return 0;
}

