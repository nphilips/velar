A Test of Text Server MarkDown Processing
=========================================
**Contents**

[TOC]

Markdown[^1] is type of structured text.

HTML is an abbreviation. Hover over it to see the definition. [[OracleTipsAndTricks]]

Apple
:   Pomaceous fruit of plants of the genus Malus in 
    the family Rosaceae.

Orange
:   The fruit of an evergreen tree of the genus Citrus.


[^1]: footie foot notes

*[HTML]: Hyper Text Markup Language

Tables
------
Value                                | Example
-------------------------------------|--------------------------
Application and Database Server Name | `TRIRIGATRN`
Database Server Name                 | `TRIRIGATRN`
Database system user/password        | `system/live3fight`
Database Port#                       | `1521`
Database SID                         | `orcl`
Database Schema/password used        | `tridata32/tridata32`

