#!/usr/bin/python2.7
import web
import glob
import os.path
import datetime
import socket
import slimmer
import codecs
import logging
import pygmentshelper
import colorsys
import random
import urllib
import json
import subprocess
import sys
import tempfile

from PIL import Image
from renderer import RendererFactory
from bread import Bread
import user_agents

from datetime import timedelta
from email.utils import parsedate
from random import choice
from slimmer import css_slimmer, js_slimmer

import settings as app_settings


logging.basicConfig()
logger = logging.getLogger('textserver')

#logger.setLevel(logging.DEBUG)

# The routing setup for URL requests
urls = (
    '/ua', 'uaprinter',
    r'/[Aa]bout', 'about',

    r'/settings/history/delete', 'setting_delete_history',
    r'/[Ss]ettings', 'settings',

    r'/favicon.ico', 'favicon',

    r'/recent.json', 'recentitems',

    r'/(.*)\.(rst|mkd|md|markdown|txt|text|textile|htm|html|log|csv)', 'page',
    r'/(.*)\.(png|jpg|gif|jpeg)', 'image',
    r'/(.*)\.(pdf|docx)', 'rawfile',
    pygmentshelper.get_all_patterns_regex(), 'page',
    r'/(.*?)', 'index',
)

images_ext = ['.png', '.jpg', '.gif', '.jpeg', '.bmp', '.svg', '.pdf', '.tif', '.tiff']

pygments_ext = pygmentshelper.get_all_dot_patterns()

documents_ext = ['.rst', '.mkd', '.md', '.markdown', '.txt', '.text', '.textile', '.htm', '.html', '.log', '.csv']

rawfiles_ext = ['.pdf', '.docx']

index_ext = documents_ext + images_ext + pygments_ext + rawfiles_ext

# This is the content root
content_root = app_settings.content_root

# Is there more than one root?
# TODO: Flatten single item lists
is_multi_root = isinstance(app_settings.content_root, list)

# TODO: This could be read directly from the settings file to
#       allow custom aliasing of the roots.
content_map = dict()

if is_multi_root:
    for root in content_root:
        if isinstance(root, tuple) and len(root) == 2:
            root_name = root[0]
            root_path = root[1]
        else:
            root = root[:-1] if root[-1] == "/" else root
            root_name = os.path.basename(root)
            root_path = root

        root_path = root_path[:-1] if root_path[-1] == "/" else root_path # Remove tailing slash
        logger.debug("Adding mult-content root {}:{}".format(root_name, root_path))
        content_map[root_name] = root_path
else:
    content_root = content_root[:-1] if content_root[-1] == "/" else content_root # Remove tailing slash

logger.debug(content_root)
logger.debug(content_map)

# Templates are found in the 'templates' directory
render = web.template.render(os.path.join(os.path.dirname(__file__), 'templates'))

# If true, we will check the If-Modified-Since header sent by the client
# (browser) and respond with a 304 Not Modified when appropriate to prevent
# unnecessary data transfers.
allow_cache = app_settings.allow_cache
web.config.debug = app_settings.debug

read_file_title_in_index = app_settings.read_file_title_in_index

# Da Application
app = web.application(urls, globals())

session = web.session.Session(app, web.session.DiskStore('sessions'), initializer={'history': list(), 'settings_alert_success': None})
session.history = []

# Handle to a long-running cygpath process. It is used to convert paths when calling external tools.
_cygpath_proc = None

def getRandomColor():
    """
    Generates a random color, with a fixed Saturation and Lightness.

    Returns a comma separated string, suitable for use with the CSS rgb() function.
    """

    h = random.random() # rnd [0, 360]
    s = 0.55            # 55  [0, 100]
    v = 0.89            # 89  [0, 100]

    # Converts [0, 1] to [0, 255]
    to_css_rgb = lambda x: str(int(round(x * 255.0)))
    return ",".join(map(to_css_rgb, colorsys.hsv_to_rgb(h, s, v)))

# Modified from https://chromium.googlesource.com/chromium/src.git/+/lkcr/tools/python/google/platform_utils_win.py
#
# This will forever run a single cygpath.exe process in the background!
def get_abs_path(path, force=False):
    """Returns an absolute windows path. If platform is cygwin, converts it to
    windows style using cygpath.
    For performance reasons, we use a single cygpath process, shared among all
    instances of this class. Otherwise Python can run out of file handles.
    """
    if not force and sys.platform != "cygwin":
      return os.path.abspath(path)

    global _cygpath_proc
    if not _cygpath_proc:
      cygpath_command = ["cygpath.exe",
                         "-a", "-m", "-f", "-"]
      _cygpath_proc = subprocess.Popen(cygpath_command,
                                       stdin=subprocess.PIPE,
                                       stdout=subprocess.PIPE)
    _cygpath_proc.stdin.write(path + "\n")
    return _cygpath_proc.stdout.readline().rstrip()

def run_pandoc(input_file, out_type, ext=None, pandoc_args=[]):
    ext = ext if ext else "." + out_type

    input_file_dir = os.path.dirname(input_file)

    input_file = get_abs_path(input_file)

    tempfd, output_file = tempfile.mkstemp(ext, dir=app_settings.tmp_folder)
    os.close(tempfd)
    output_file = get_abs_path(output_file)

    subprocess.check_call([app_settings.pandoc_bin, "-t", out_type, "-o", output_file, input_file] + pandoc_args, cwd=input_file_dir)

    with open(output_file, mode='rb') as f:
        data = f.read()

    # Cleanup the temp file
    os.remove(output_file)

    return data

def run_editor(input_file, extra_args=[]):
    """
    Runs a text editor when running as an interactive process. This will not work when running as a service.
    :param input_file: The file to edit
    :param extra_args: Any additional arguments to pass to the editor
    """
    if app_settings.editor_home:
        input_file = get_abs_path(input_file)
        home_abs = get_abs_path(app_settings.editor_home)
        subprocess.call([app_settings.editor_bin, input_file] + extra_args, env={"HOME": home_abs})
    else:
        logger.debug("No editor configured. Ignoring edit request.")

def store_hit(history, path, name=None):
    logger.debug(str(history))

    if not name:
        name = os.path.basename(path)

    item = pagedata(name, "/" + path)

    if item in history:
        history.remove(item)

    # Fails if item is not ASCII
    #logger.debug("Storing {}".format(item))
    history.insert(0, item)


def clear_top_hits():
    pass

#
# -- Sessions --
#

def get_session_item(key, clear=False):
    return getattr(web.ctx.session, key)


def put_session_item(key, value):
    logger.debug("Session Store: {}={}".format(key, value))
    setattr(web.ctx.session, key, value)

#
# Standard error pages
#

def notfound():
    return web.notfound(render.error404(render.navbar(), randomcolor=getRandomColor()))

def internalerror():
    return web.notfound(render.serverError("Unknown server error occured.", render.navbar(), randomcolor=getRandomColor()))

app.notfound = notfound
app.internalerror = internalerror

class favicon:
    def GET(self):
        try:
            web.header("Content-Type", "application/json")
            f = open("static/favicon.ico", 'rb')
            return f.read()
        except:
            pass

class recentitems:
    def GET(self):
        web.header("Content-Type", "application/json")
        hits = []
        try:
            for hit in session.history:
                hits.append(hit.__dict__)
        except AttributeError:
            pass

        return json.dumps(hits)

class settings:
    def GET(self):
        alert_success = session.settings_alert_success
        session.settings_alert_success = None
        breadcrumb = Bread("/Settings").links
        return render.settings(render.navbar(at_settings="active",
            recent_items=session.history), "Settings",
            render.breadcrumbbar(breadcrumb), alert_success=alert_success,
            randomcolor=getRandomColor())

class setting_delete_history:
    def POST(self):
        session.settings_alert_success = "<strong>Success</strong>: What is forgotten is doomed to be repeated."
        session.history = []
        raise web.redirect('/settings')

# The about page. Nothing facy here, we render the README.rst in the parent dir.
class about:
    def GET(self):
        readme_file = os.path.join(os.path.dirname(os.path.realpath(__file__)), "..", "README.rst")

        user_data = web.input()
        render_mode = "normal"

        try:
            render_mode = user_data.format
        except AttributeError:
            render_mode = "normal"

        if render_mode == "raw":
            logger.debug("Rendering the content RAW.")
            f = codecs.open(readme_file, mode='r', encoding="utf-8")
            return f.read()

        renderer = RendererFactory.newRenderer(os.path.splitext(readme_file)[1])
        readmefilename = os.path.basename(readme_file)
        title, readme = renderer.getHtmlFromFile(readme_file)
        title = "About TextServer"
        breadcrumb = Bread("/About").links
        return render.page(render.navbar(at_about="active",
            recent_items=session.history), title, readme,
            render.breadcrumbbar(breadcrumb), True,
            randomcolor=getRandomColor())

# This is the index page. It lists all available markdown or rst files in a directory.
class index:
    def GET(self, url):

        if not content_root or content_root is '':
            return render.serverError("Content root directory is not set. Edit <code>settings.py</code> to set the value and restart the server.", render.navbar(), randomcolor=getRandomColor())

        # Prefix the url part with a slash so that the
        # breadcrumb can start with the home page.
        at_home = None
        if len(url) <= 0:
            at_home = "active"
        else:
            url = "/" + url

        logger.debug("AT_HOME = {}".format(at_home))

        pages = []
        directories = []
        readme_file = None

        if  is_multi_root and at_home:
            for key, value in content_map.iteritems():
                directories.append(pagedata(key, key))
        else:
            # Turn URL into an actual filesystem path.
            root, subdirectory = get_file_path(url)

            logger.debug("About to access " + subdirectory)

            # Directory does not exist. Four-O-Four.
            if subdirectory is None or not os.path.isdir(subdirectory):
                web.notfound()
                return render.error404(render.navbar(), randomcolor=getRandomColor())
            # Filter the list of files and remove anything we cannot handle.
            for item in self.get_files(glob.glob(subdirectory + '/*'), root):
                if item[0] == "doc":
                    pages.append(item[1])
                else:
                    directories.append(item[1])

            # Sort files by name
            pages = sorted(pages, key=lambda p:p.Name)

            # Get any readme files in this directory. We will render it inline with the index.
            readme_file = get_first_file((os.path.join(subdirectory, "README.md"),
                                          os.path.join(subdirectory, "README.markdown"),
                                          os.path.join(subdirectory, "README.mkd"),
                                          os.path.join(subdirectory, "README.txt"),
                                          os.path.join(subdirectory, "README.rst")))

        # Generate breadcrumbs for el rapido navigation.
        breadcrumb = None
        if(len(url) > 0):
            breadcrumb = Bread(url).links

        # Read and render README file.
        if readme_file is None:
            readme = None
            readmefilename = None
        else:
            renderer = RendererFactory.newRenderer(os.path.splitext(readme_file)[1])
            readmefilename = os.path.basename(readme_file)
            title, readme = renderer.getHtmlFromFile(readme_file)

        # Render output
        my_top_list = session.history
        if not at_home:
            my_top_list = None

        return render.index(render.navbar(at_home=at_home, recent_items=session.history),
                                          socket.gethostname(),
                                          pages,
                                          directories,
                                          render.breadcrumbbar(breadcrumb),
                                          readme,
                                          readmefilename,
                                          my_top_list,
                                          getRandomColor())

    def get_files(self, globs, root):
        for textfile in globs:
            name = os.path.relpath(textfile, root)
            if os.path.isfile(textfile):
                extension = os.path.splitext(textfile)[1].lower()
                if extension not in index_ext:
                    logger.debug("Skipping unknown extension " + extension)
                    continue

                # We will never try to read the title from the contents of html or text files.
                if not read_file_title_in_index or (extension == ".html" or extension == ".htm" or extension == ".txt"):
                    head = os.path.basename(name)
                else:
                    head = readfiletitle(textfile)

                attributes = None

                # Direct links to image files will get the image raw,
                # add the format attribute render it embedded in a stup.
                if extension in images_ext:
                    attributes = {'format': 'fancy'}

                yield "doc", pagedata(head, name, attributes)
            else:
                yield "dir", pagedata(os.path.basename(name), name)

class BaseFileHandler:

    def combine_file(self, file_list):
        content = ""
        for file_item in file_list:
            # Try to open the text file, returning a 404 upon failure
            f = open(file_item, 'r')
            content += f.read() + '\n'

        return content

    def combine_css(self, file_list):
        return css_slimmer(self.combine_file(file_list))

    def combine_js(self, file_list):
        return js_slimmer(self.combine_file(file_list))

    def get_file_modified_date(self, file_path):
        return datetime.datetime.utcfromtimestamp(os.path.getmtime(file_path))


    def check_if_file_modified(self, file_path):
        file_last_modified = self.get_file_modified_date(file_path)
        if_modified_since = web.ctx.env.get('HTTP_IF_MODIFIED_SINCE', None)

        logger.debug("File Date: {}".format(file_last_modified))

        # Once Live.js reload the page, the if_modified_since header will no longer be sent.

        # Check if the file was modified since the browser last retrieved it
        if allow_cache and not if_modified_since is None:
            if_modified_since_date = parse_http_datetime(if_modified_since)
            logger.debug("Client Cache Date: {}".format(if_modified_since_date))
            if file_last_modified - if_modified_since_date < datetime.timedelta(seconds=5):
                logger.debug("NOT MODIFIED")
                web.notmodified()
                return True

        return False

    def HEAD(self, url, extension):

        extension = "." + extension

        url_with_ext = url + extension
        root, file_path = get_file_path(url_with_ext)

        try:
            # Probably bad practice... but needed for Live.js
            web.header("Content-Length", os.path.getsize(file_path))
            web.header("Content-Type", get_content_type(extension))

            self.check_if_file_modified(file_path)
        except IOError:
            web.notfound()
            logger.debug("Not Found")
            return render.error404(render.navbar(recent_items=session.history), randomcolor=getRandomColor())

# This class renders a file.
class image(BaseFileHandler):

    def GET(self, url, extension):
        user_data = web.input()

        url_with_ext = url + "." + extension
        root, page_file = get_file_path(url_with_ext)
        real_url = '/%s' % url_with_ext

        try:
            web.lastmodified(self.get_file_modified_date(page_file))
            if self.check_if_file_modified(page_file):
                return
        except OSError:
            web.notfound()
            return render.error404(render.navbar(recent_items=session.history), randomcolor=getRandomColor())

        store_hit(session.history, url_with_ext)
        session.history = session.history[:app_settings.history_max] # Max items

        try:
            if user_data.format == "fancy":
                breadcrumb = None
                if(len(url) > 0):
                    breadcrumb = Bread("/" + url_with_ext).links

                try:
                    img = Image.open(page_file)
                    size = "%s x %s" % img.size
                    image_details = "%s (%s)" % (os.path.basename(url_with_ext), size)
                    return render.image(render.navbar(True, recent_items=session.history), "Image", real_url, image_details, render.breadcrumbbar(breadcrumb))
                except IOError:
                    web.notfound()
                    return render.error404(render.navbar(recent_items=session.history), randomcolor=getRandomColor())
        except AttributeError:
            pass

        logger.debug("Rendering image RAW.")
        f = open(page_file, 'rb')
        return f.read()

# This class allows acces to raw files (pdf etc).
class rawfile(BaseFileHandler):

    def GET(self, url, extension):
        user_data = web.input()

        url_with_ext = url + "." + extension
        root, page_file = get_file_path(url_with_ext)
        real_url = '/%s' % url_with_ext

        try:
            web.lastmodified(self.get_file_modified_date(page_file))
            if self.check_if_file_modified(page_file):
                return
        except OSError:
            web.notfound()
            return render.error404(render.navbar(recent_items=session.history), randomcolor=getRandomColor())

        store_hit(session.history, url_with_ext)
        session.history = session.history[:app_settings.history_max] # Max items

        logger.debug("Rendering file RAW.")
        f = open(page_file, 'rb')
        return f.read()

# This class renders a page.
class page(BaseFileHandler):

    def GET(self, url, extension):
        ismarkdownfile = True
        user_data = web.input()
        extension = ".%s" % extension.lower()

        url_with_ext = url + extension
        root, page_file = get_file_path(url_with_ext)

        if root is None:
            web.notfound()
            return render.error404(render.navbar(recent_items=session.history), randomcolor=getRandomColor())

        page_file_name = os.path.basename(page_file)
        page_file_name = os.path.splitext(page_file_name)[0]

        logger.debug("Page File Name: {}".format(page_file_name))

        launch_editor = False

        try:
            launch_editor = user_data.editor
        except AttributeError:
            launch_editor = False

        if launch_editor:
            run_editor(page_file)


        is_document = (extension) in documents_ext

        try:
            web.lastmodified(self.get_file_modified_date(page_file))
            if self.check_if_file_modified(page_file):
                # Update the top hits anyways
                store_hit(session.history, url_with_ext)
                session.history = session.history[:app_settings.history_max] # Max items
                return
        except OSError:
            web.notfound()
            return render.error404(render.navbar(recent_items=session.history), randomcolor=getRandomColor())



        render_mode = "normal"

        try:
            render_mode = user_data.format
        except AttributeError:
            render_mode = "normal"

        if render_mode == "raw":
            logger.debug("Rendering the content RAW.")
            f = codecs.open(page_file, mode='r', encoding="utf-8")
            return f.read()

        logger.debug("Continue Normal Rendering")

        try:
            renderer = RendererFactory.newRenderer(extension)
            title, content = renderer.getHtmlFromFile(page_file)
            if title is None or len(title) == 0:
                title = readfiletitle(page_file)
        except UnicodeDecodeError as e:
            return render.serverError("Bad file encoding. Use a text editor to change the encoding to UTF-8 and try again.<br />%s" % str(e), render.navbar(), randomcolor=getRandomColor())
        except ValueError as e:
            return render.serverError(str(e), render.navbar(), randomcolor=getRandomColor())

        ua_string = web.ctx.env['HTTP_USER_AGENT']
        user_agent = user_agents.parse(ua_string)
        mobile = not user_agent.is_pc

        logger.debug("Mobile? {}".format(mobile))

        if render_mode == "standalone":
            stylesheet = self.combine_css(('static/pygments.css', 'static/bootstrap/css/bootstrap.min.css', 'static/bootstrap/css/bootstrap-theme.min.css', 'static/style.css', 'static/style-standalone.css', 'static/font-awesome-4.0.3/css/font-awesome.cdn.css'))
            script = self.combine_js(('static/bootstrap/js/jquery-1.10.2.min.js', 'static/bootstrap/js/bootstrap.min.js', 'static/textserver.js'))
            return render.pagestandalone(title, content, stylesheet, script, is_document)
        elif render_mode == "docx":
            web.header("Content-Type", "application/vnd.openxmlformats-officedocument.wordprocessingml.document")
            web.header("content-disposition", "attachment;filename=\"" + page_file_name + ".docx\"")
            return run_pandoc(page_file, "docx")

        elif render_mode == "tex":
            web.header("Content-Type", "application/x-latex")
            web.header("content-disposition", "attachment;filename=\"" + page_file_name + ".tex\"")
            return run_pandoc(page_file, "latex", ".tex")

        elif render_mode == "pdf":
            web.header("Content-Type", "application/pdf")
            web.header("content-disposition", "attachment;filename=\"" + page_file_name + ".pdf\"")
            return run_pandoc(page_file, "pdf", ".pdf", ["--pdf-engine=xelatex"])

        elif render_mode == "jira":
            web.header("Content-Type", "text/plain")
            web.header("content-disposition", "attachment;filename=\"" + page_file_name + ".txt\"")
            return run_pandoc(page_file, "jira", ".txt")

        elif render_mode == "mkd":
            web.header("Content-Type", "text/markdown")
            web.header("content-disposition", "attachment;filename=\"" + page_file_name + ".mkd\"")
            return run_pandoc(page_file, "markdown", ".mkd")

        elif render_mode == "rst":
            web.header("Content-Type", "text/x-rst")
            web.header("content-disposition", "attachment;filename=\"" + page_file_name + ".rst\"")
            return run_pandoc(page_file, "rst", ".rst")

        else:
            breadcrumb = None
            if(len(url) > 0):
                breadcrumb = Bread("/" + url_with_ext).links

            store_hit(session.history, url_with_ext, title)
            session.history = session.history[:app_settings.history_max] # Max items
            return render.page(render.navbar(True,
                recent_items=session.history), title, content,
                render.breadcrumbbar(breadcrumb), is_document,
                randomcolor=getRandomColor(), mobile=mobile)

class uaprinter:
    def GET(self):
        return 'your user-agent is ' + web.ctx.env['HTTP_USER_AGENT']

class pagedata:
    Name = ""
    Path = ""

    def __init__(self, name, path, attributes=None):
        self.Name = name
        self.Path = urllib.quote(path)
        if attributes:
            self.Path += "?" + urllib.urlencode(attributes)

    def __eq__(self, another):
        return hasattr(another, 'Path') and self.Path == another.Path

    def __hash__(self):
        return hash(self.Path)

    def __str__(self):
        return "%s, %s" % (self.Name, self.Path)


def get_content_type(extension):
    return "text/html"

def readfiletitle(filename):
    try:
        with open(filename) as myfile:
            line1=myfile.next()
            line2=myfile.next()
            if line2.startswith("="):
                head = line1
            else:
                head = os.path.basename(filename)
    except:
        head = os.path.basename(filename)

    return head

def parse_http_datetime( s ):
    return datetime.datetime(*parsedate(s)[:6])

# Given a list of files, returns the first file that exists.
def get_first_file (file_list):
    for file_item in file_list:
        if os.path.isfile(file_item):
            return file_item
    return None

def get_file_path(subpath):
    if is_multi_root:
        subpathparts = subpath.split("/")
        subpathparts = [x for x in subpathparts if x]
        root_name = subpathparts[0]
        if not root_name in content_map:
            return None, None
        subpath_remainer = subpathparts[1:]
        logger.debug("Resolving path for multi-root: {} == {}".format(root_name, subpath_remainer))
        actual_path = content_map[root_name] + '/' + '/'.join(subpath_remainer)
        return os.path.dirname(content_map[root_name]), actual_path
    else:
        return content_root, content_root + '/' + subpath

if __name__ == '__main__':
    # We use templates and other resources located relative to the script
    # cd to the script dir to be able to use them.
    cwd = os.path.dirname(os.path.realpath(__file__))
    logger.debug("Changing directory to {0}".format(cwd))
    os.chdir(cwd)
    app.run()
