import os.path

# From https://bitbucket.org/russellballestrini/bread/raw/tip/bread.py
class Bread( object ):
    """
    Create Bread object
    ----------------------------------------
    >>> from bread import Bread
    >>> bread = Bread( "http://www.foxhop.net/samsung/HL-T5087SA/red-LED-failure" )
    >>> print bread.crumbs
    >>> print bread.links

    """
    def __init__( self, uri=None ):
        if uri: self.uri = uri # invoke uri.setter
        else: self._uri = uri  # set _uri to none

    @property
    def uri( self ):
        return self._uri

    @uri.setter
    def uri( self, uri ):
        """set the _uri, _protocol, and crumbs attributes"""
        self._protocol = 'http://'
        protocols = ['http://', 'https://', 'ftp://', 'sftp://']
        for protocol in protocols:
            if uri.startswith( protocol ): 
                self._protocol = protocol
                uri = uri[ len(protocol): ] # remove protocol from uri

        self._uri = uri.rstrip('/')
        self.crumbs = self._uri.split('/')

    @property
    def links( self ):
        links = []
        length = len(self.crumbs)
        for count, crumb in enumerate( self.crumbs, start = 1 ):
            crumb_uri =  '/'.join( self.crumbs[ 0:count ] )

            #crumb, ext = os.path.splitext(crumb)

            # Assume an empty item in the crumb list is the index/home and fill that in.
            ccrumb = crumb if(len(crumb) > 0) else "Home"
            ccrumb_uri = crumb_uri if (len(crumb_uri) > 0) else "/"
            if count == length:
                links.append(ccrumb)
            else:
                links.append( '<a title="Navigate to ' + ccrumb + '" href="' + ccrumb_uri + '" class="breadcrumb">' + ccrumb + '</a>' )
        return links

