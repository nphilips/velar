#!/usr/bin/python

import re
from pygments import lexers

def get_all_patterns():
    all_patterns = ()
    for lexer in lexers.get_all_lexers():
        for pattern in lexer[2]:
            yield pattern[2:] if pattern.startswith('*.') else pattern

def get_all_patterns_regex():
    lst = list(get_all_patterns())
    lst = map(re.escape, lst)
    return r'/(.*)\.(' + '|'.join(lst) + r')'

def get_all_dot_patterns():
    llst = list(get_all_patterns())
    llst = map(lambda x: x if x.startswith('.') else '.%s' % x, llst)
    return llst
