import markdown
import codecs
import os.path
import textile
import pygmentshelper
import re

from pygments import highlight, lexers
from pygments.formatters import HtmlFormatter
from docutils import core
from docutils.writers.html4css1 import Writer,HTMLTranslator
from types import TypeType

class Renderer(object):
    """
    This is the base Renderer object.  To implement a new renderer, inherit
    from this class and advertise the file extensions you support by
    implementing the handlesExtension method
    """
    @staticmethod
    def handlesExtension(extension):
        """
        Gets whether a given extension is supported by this renderer.

        Expects extensions in the '.ext' format.
        """
        return False

    def getHtmlFromFile(self, text_file):
        """
        A wrapper around the getHtml function that can read the markup
        text from a file.
        """
        savedPath = os.getcwd()
        try:
            # the file may include other files, located relative to the source. 
            # cd, so the parser can find them.
            f = codecs.open(text_file, mode='r', encoding="utf-8")
            content = f.read()
            return self.getHtml(content, text_file)
        finally:
            pass

class HtmlRenderer(Renderer):
    """
    Renders
    """
    @staticmethod
    def handlesExtension(extension):
        return extension.lower() in (".html", ".htm", ".html")

    def getHtml(self, markup_text, filename):
        # We have nothing to do
        return "", markup_text

class PlainTextRenderer(Renderer):
    @staticmethod
    def handlesExtension(extension):
        return extension.lower() in (".txt", ".text", ".log", ".csv")

    def getHtml(self, markup_text, filename):
        # We have nothing to do
        filename = os.path.basename(filename)
        #<div class="filenameheader"><p>%s</p></div> % filename
        return "", '<pre class="plaintext">%s</pre>' % markup_text

class TextileRenderer(Renderer):
    @staticmethod
    def handlesExtension(extension):
        return extension.lower() in (".textile")

    def getHtml(self, markup_text, filename):
        return "", textile.textile(markup_text)

class MarkdownRenderer(Renderer):
    @staticmethod
    def handlesExtension(extension):
        return extension.lower() in (".mkd", ".md", ".markdown")

    def getHtml(self, markup_text, filename):
        md = markdown.Markdown(output_format='html5', extensions=['extra', 'codehilite', 'meta', 'toc', 'wikilinks' ])
        text = md.convert(markup_text)
        # This is very hackish, but it's the best we can do...
        text = re.sub(r'\<table\>', r'<table class="table table-bordered">', text)
        return "", text

class RestRenderer(Renderer):
    @staticmethod
    def handlesExtension(extension):
        extension = extension.lower()
        if extension == ".rst":
            return True
        return False

    def getHtml(self, markup_text, filename):
        # This ensures that only the title is h1. Make the 
        # HTML look similar to MarkDown output.
        overrides = { 'initial_header_level': '2',
                      'table_style' : 'table table-bordered',
                      'syntax_highlight' : 'short'}

        parts = core.publish_parts(
                                source=markup_text,
                                source_path=filename,
                                writer_name='html',
                                settings_overrides=overrides)
        body = parts['body']
        body = re.sub(r'<li>\s\*\[\s?\]', r'<li class="task-list"><input type="checkbox" disabled="">', body)
        body = re.sub(r'<li>\s\*\[x\]', r'<li class="task-list"><input type="checkbox" checked="checked" disabled="">', body)

        body = re.sub(r'(\[\s?\])', r'<input type="checkbox" disabled="">', body)
        body = re.sub(r'(\[x\])', r'<input type="checkbox" checked="checked" disabled="">', body)

        return parts['title'], parts['body_pre_docinfo'] + body

class PygmentsRenderer(Renderer):

    extensions = pygmentshelper.get_all_dot_patterns()

    @staticmethod
    def handlesExtension(extension):
        extension = extension.lower()
        return extension in PygmentsRenderer.extensions

    def getHtml(self, markup_text, filename):
        lexer = lexers.get_lexer_for_filename(filename)
        filename = os.path.basename(filename)
        formatter = HtmlFormatter()
        return "", '<div class="filenameheader"><p>%s (%s)</p></div>' % (filename, lexer.name) + ' <div class="pygmented">' + highlight(markup_text, lexer, formatter) + '</div>'

class RendererFactory(object):
    @staticmethod
    def newRenderer(extension):
        # Walk through all Renderer classes 
        rendererClasses = [j for (i,j) in globals().iteritems() if isinstance(j, TypeType) and issubclass(j, Renderer)]
        for rendererClass in rendererClasses :
            if rendererClass.handlesExtension(extension):
                selectedRen = rendererClass()
                print "Selected renderer %s for %s" % (type(selectedRen), extension)
                return selectedRen
        #if search was unsuccessful, raise an error
        raise ValueError('No renderer that can handle "%s" files.' % extension)

