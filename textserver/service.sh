#!/bin/bash
SERVICE_NAME="TextServer"
DISPLAY_NAME="Text Server"
DESCRIPTION="A simple web server that will serve markup text files as HTML pages."

cd `dirname "$0"`
DIR_NAME=`pwd`

case $1 in
    install )
        cygrunsrv --install "$SERVICE_NAME" -d "$DISPLAY_NAME" -p "/bin/python2" -a "\"$DIR_NAME/app.py\" 127.0.0.1" \
            --stdout "$DIR_NAME/$SERVICE_NAME.stdout.log" --stderr "$DIR_NAME/$SERVICE_NAME.stderr.log" \
            --desc "$DESCRIPTION"
        ;;
    remove )
        cygrunsrv --remove "$SERVICE_NAME"
        ;;
    * )
        echo "Specify a command. One of ('install' or 'remove'). Remember to start the shell as System administrator."
        ;;
esac

