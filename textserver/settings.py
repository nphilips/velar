# This is the content root.
# It can be a string for a single root:
#
#       content_root = '/home/alice'
#
# The home page will then list the contents of '/home/alice'
#
# OR it could be a list:
#
#       content_root = ['/home/alice/work', '/home/alice/documents']
#
# OR a list of strings and tuples:
#
#       content_root = ['/home/alice/work', '/home/alice/documents', ('private', '/home/alice/private/documents')]
#
# When the root is a list, the home page will list just the roots, in this case 'work', 'documents' and 'private'.
#
# * If item names are not unique use a tuple to set a unique key.
# * Create a launcher configuration entry for each root:
#
#      [textserver0]
#      url=http://127.0.0.1:8080/work
#      fileroot=/home/alice/work
#
#      [textserver1]
#      url=http://127.0.0.1:8080/documents
#      fileroot=/home/alice/documents
#
#      [textserver2]
#      url=http://127.0.0.1:8080/private
#      fileroot=/home/alice/private/documents
#
#
content_root = ['/cygdrive/c/Users/Nithin/Documents/Work/', '/cygdrive/c/Users/Nithin/Desktop/', ('Home', '/home/nithin')]

# When converting documents using pandoc, this directory is used to 
# store temporary files. Must be writeable.
tmp_folder='/cygdrive/c/tools/velar/temp'

# If true, we will attempt to read the file title by looking at its content. If
# false, it'll use the file name only.
read_file_title_in_index = False

# Set to True to enable debugging. Debugging disable all caches.
debug = False

# The maximum number of items to keep in the history
history_max = 10

# If true, we will check the If-Modified-Since header sent by the client
# (browser) and respond with a "304 Not Modified" when appropriate to prevent
# unnecessary data transfers.
allow_cache = True

# The path to the pandoc binary
pandoc_bin="/cygdrive/c/Program Files/Pandoc/pandoc.exe"

# The path to the text editor
editor_bin="/cygdrive/c/Program Files (x86)/Vim/vim81/gvim.exe"
# The HOME directory for the editor. This is used to set the HOME environment variable.
editor_home="/cygdrive/c/Users/Nithin"
